import datetime
import os
from buildbot.plugins import *

def setup_workers(c):
    # Use port 9989 for worker connections.
    # TODO: This needs to be secured.
    c['protocols'] = {'pb': {'port': 9989}}

    # Workers must be added in site.py
    c['workers'] = []

def setup_change_sources(c):
    c['change_source'] = [
        changes.GitPoller(
            'git@gitlab.com:oryx/oryx.git',
            branches=True,
            pollInterval=120,
            buildPushesWithNoCommits=True,
        )
    ]

def setup_schedulers(c):
    builds_to_trigger = [
        'docs',
        'qemux86',
        'qemux86-64',
        'qemuarm',
        'qemuarm64',
        'raspberrypi3',
        'raspberrypi3-64',
        'raspberrypi4',
        'raspberrypi4-64',
    ]
    c['schedulers'] = [
        schedulers.AnyBranchScheduler(
            name='branch-trigger',
            treeStableTimer=120,
            builderNames=builds_to_trigger
        ),
        schedulers.Nightly(
            name='nightly-trigger',
            hour=0, minute=5,
            builderNames=['nightly-trigger']
        )
    ]

def add_steps_setup(factory):
    factory.addStep(steps.Git(
        name='Fetch',
        repourl='git@gitlab.com:oryx/oryx.git',
        submodules=True,
        progress=True,
        mode='full'
    ))
    factory.addStep(steps.ShellCommand(
        name='Apply patches',
        command=['./patches/apply.sh']
    ))

def add_steps_publish(factory):
    factory.addStep(steps.DirectoryUpload(
        name='Publish',
        workersrc='build/images',
        masterdest=util.Interpolate('/srv/oryx/bb/%(prop:branch)s'),
        url=util.Interpolate('https://downloads.oryx-linux.org/bb/%(prop:branch)s')
    ))

def add_steps_teardown(factory):
    factory.addStep(steps.ShellSequence(
        name='Clean up',
        commands=[
            util.ShellArg(command=['rm', '-rf', 'build/tmp']),
            util.ShellArg(command=['rm', '-rf', 'build/cache']),
            util.ShellArg(command=['rm', '-rf', 'docs/_build'])
        ]
    ))

def setup_builder_docs():
    factory = util.BuildFactory()
    add_steps_setup(factory)
    factory.addStep(steps.ShellCommand(
        name='Build docs',
        command=['./scripts/build.py', '--docs', '--no-bitbake']
    ))
    add_steps_publish(factory)
    add_steps_teardown(factory)
    factory.addStep(steps.MasterShellCommand(
        name='Unpack docs on master',
        command=[
            'tar',
            'xzf', util.Interpolate('/srv/oryx/bb/%(prop:branch)s/docs/oryx-docs-html.tar.gz'),
            '-C', util.Interpolate('/srv/oryx/bb/%(prop:branch)s/docs'),
            '--transform=s/^oryx-docs-//'
        ]
    ))

    return util.BuilderConfig(
        name='docs',
        workernames=['build0'],
        factory=factory
    )

def setup_builder_images(machine):
    factory = util.BuildFactory()
    add_steps_setup(factory)
    factory.addStep(steps.ShellCommand(
        name='Build guest images',
        command=[
            './scripts/build.py', '-k', '--rm-work',
            '--dl-dir', '/home/buildbot/ci-cache/downloads',
            '--sstate-dir', '/home/buildbot/ci-cache/sstate-cache',
            '-V', util.Property('scheduler'),
            '-M', machine,
            '-S', 'guest',
            '-A', 'minimal',
            ]
    ))
    factory.addStep(steps.ShellCommand(
        name='Build native images',
        command=[
            './scripts/build.py', '-k', '--rm-work',
            '--dl-dir', '/home/buildbot/ci-cache/downloads',
            '--sstate-dir', '/home/buildbot/ci-cache/sstate-cache',
            '-V', util.Property('scheduler'),
            '-M', machine,
            '-S', 'native',
            '-A', 'host-test',
            ]
    ))
    add_steps_publish(factory)
    add_steps_teardown(factory)

    return util.BuilderConfig(
        name=machine,
        workernames=['build0'],
        factory=factory
    )

@util.renderer
def r_date(props):
    return datetime.date.today().isoformat()

def setup_builder_nightly_trigger():
    with open(os.path.expanduser('~/.ssh/id_ed25519'), 'r') as f:
        sshkey = f.read()

    factory = util.BuildFactory()
    factory.addStep(steps.SetProperty(
        name='Set date',
        property='date',
        value=util.Interpolate('%(kw:r_date)s', r_date=r_date)
    ))
    factory.addStep(steps.Git(
        name='Fetch',
        repourl='git@gitlab.com:oryx/oryx.git',
        branch='master',
        submodules=True,
        progress=True,
        mode='full'
    ))
    factory.addStep(steps.ShellCommand(
        name='Update',
        command=['git', 'submodule', 'update', '--init', '--remote']
    ))
    factory.addStep(steps.ShellSequence(
        name='Commit',
        commands=[
            util.ShellArg(
                command=['git', 'update-ref', 'refs/heads/nightly', 'HEAD'],
                logfile='update-ref'),
            util.ShellArg(
                command=['git', 'checkout', 'nightly'],
                logfile='checkout'),
            util.ShellArg(
                command=['git', 'commit', '-a', '-m', util.Interpolate('Nightly snapshot: master %(prop:date)s')],
                logfile='commit'),
            util.ShellArg(
                command=['git', 'tag', '--force', util.Interpolate('nightly/master/%(prop:date)s')],
                logfile='tag',
                )
        ]
    ))
    factory.addStep(steps.GitPush(
        name='Push branch',
        repourl='git@gitlab.com:oryx/oryx.git',
        branch='nightly',
        force=True,
        sshPrivateKey=sshkey
    ))
    factory.addStep(steps.GitPush(
        name='Push tag',
        repourl='git@gitlab.com:oryx/oryx.git',
        branch=util.Interpolate('nightly/master/%(prop:date)s'),
        force=True,
        sshPrivateKey=sshkey
    ))

    return util.BuilderConfig(
        name='nightly-trigger',
        workernames=['build0'],
        factory=factory
    )

def setup_builders(c):
    c['builders'] = [
        setup_builder_docs(),
        setup_builder_nightly_trigger()
    ]

    machine_list = [
        'qemux86',
        'qemux86-64',
        'qemuarm',
        'qemuarm64',
        'raspberrypi3',
        'raspberrypi3-64',
        'raspberrypi4',
        'raspberrypi4-64',
    ]
    for machine in machine_list:
        c['builders'].append(setup_builder_images(machine))

def setup_services(c):
    # Services must be added in site.py
    c['services'] = []

def setup_web(c):
    c['title'] = 'Oryx CI'
    c['titleURL'] = 'https://www.oryx-linux.org/'
    c['buildbotURL'] = 'https://bb.oryx-linux.org/'
    c['www'] = {
        'port': 8010,
        'authz': util.Authz(allowRules=[util.AnyControlEndpointMatcher(role="admins")],roleMatchers=[]),
        'plugins': {
            'console_view': {}
        }
    }
    c['buildbotNetUsageData'] = 'basic'

def setup_db(c):
    c['db'] = {
        'db_url': 'sqlite:///state.sqlite'
    }

def setup():
    c = {}
    setup_workers(c)
    setup_change_sources(c)
    setup_schedulers(c)
    setup_builders(c)
    setup_services(c)
    setup_web(c)
    setup_db(c)
    return c
